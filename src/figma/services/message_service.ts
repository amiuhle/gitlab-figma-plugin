import { getAccessToken, getCurrentSelection } from '../utils/figma_utils';
import { FIGMA_MESSAGE_TYPES } from '~/shared/constants';

/**
 * @name MessageService
 * @description The Message service provides an API for sending messages to the plugin API (our Vue application).
 * Available methods are simple wrappers around `figma.ui.postmessage`.
 */
export default function getMessageService(): MessageService {
  function updateSelection(): void {
    const selection = getCurrentSelection();

    figma.ui.postMessage({
      type: FIGMA_MESSAGE_TYPES.UPDATE_SELECTION,
      data: {
        selection: {
          frames: selection.frames,
          components: selection.components,
        },
      },
    });
  }

  async function updateAccessToken(): Promise<void> {
    const accessToken = await getAccessToken();

    figma.ui.postMessage({
      type: FIGMA_MESSAGE_TYPES.SET_ACCESS_TOKEN,
      data: {
        accessToken,
      },
    });
  }

  return { updateSelection, updateAccessToken };
}

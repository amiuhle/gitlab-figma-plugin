import { setAccessToken } from '../../../utils/figma_utils';

export default function reset(): void {
  setAccessToken(null).then(() => {
    figma.closePlugin('GitLab plugin was restored to its default state.');
  });
}

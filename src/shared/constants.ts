export const FIGMA_MESSAGE_TYPES = {
  UPDATE_SELECTION: 'UPDATE_SELECTION',
  SET_ACCESS_TOKEN: 'SET_ACCESS_TOKEN',
  EXPORT_SELECTION: 'EXPORT_SELECTION',
  RESIZE: 'RESIZE',
  NOTIFY: 'NOTIFY',
};

export const DEFAULT_WINDOW_SIZE = {
  width: 380,
  height: 500, // TODO this should be 460px. Will update when we move to a css-grid layout
};

export const GITLAB_INSTANCE_PROTOCOL = process.env.GITLAB_INSTANCE_PROTOCOL || 'https:';
export const GITLAB_INSTANCE_HOSTNAME = process.env.GITLAB_INSTANCE_HOSTNAME || 'gitlab.com';
export const GITLAB_BASE_URL = `${GITLAB_INSTANCE_PROTOCOL}//${GITLAB_INSTANCE_HOSTNAME}`;
export const GITLAB_GRAPHQL_BASE_URL = `${GITLAB_BASE_URL}/api/graphql`;

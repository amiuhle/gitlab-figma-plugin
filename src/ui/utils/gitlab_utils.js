import { GITLAB_INSTANCE_PROTOCOL, GITLAB_INSTANCE_HOSTNAME } from '~/shared/constants';

const GITLAB_ISSUE_URL_REGEX = /\/(.+)\/-\/issues\/([0-9]+)/;

function sanitiseFilename(filename) {
  const sanitisedFileName = filename.replace('/', ':');
  return sanitisedFileName;
}

export function pngFileFromData(data, filename) {
  const fileData = new Blob([data], { type: 'image/png' });
  const file = new File([fileData], `${sanitiseFilename(filename)}.png`, {
    type: 'image/png',
  });

  return file;
}

/**
 * Parse a GitLab issue URL.
 * @param {String} issueUrl url to test
 * @return {Object} Object containing [projectPath] and [issueId]. If invalid URL, empty object.
 */
export function parseIssueUrl(issueUrl) {
  let url;
  try {
    url = new URL(issueUrl);
  } catch (e) {
    return {};
  }

  if (url.protocol !== GITLAB_INSTANCE_PROTOCOL || url.hostname !== GITLAB_INSTANCE_HOSTNAME) {
    return {};
  }

  const match = url.pathname.match(GITLAB_ISSUE_URL_REGEX);
  if (!match) return {};

  const [, projectPath, issueId] = match;
  return {
    projectPath,
    issueId,
  };
}

/**
 * Return "normalised" issue data structure from given [issue] object
 * @param {*} issue issue data (REST or GraphQL)
 * @param {*} additionalProperties extra data to add to [issue]
 */
export function toIssue(issue, { projectId } = {}) {
  const { iid, title, state } = issue;

  const webUrl = issue.webUrl || issue.web_url;
  const { projectPath } = parseIssueUrl(webUrl);

  return {
    iid,
    title,
    state,
    webUrl,
    projectId,
    projectPath,
    fullReference: issue.reference ? `${projectPath}${issue.reference}` : issue.references?.full,
  };
}

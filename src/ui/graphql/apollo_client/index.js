import { ApolloClient } from 'apollo-client';
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import resolvers from './resolvers';
import typeDefs from '../typedefs.graphql';
import accessTokenQuery from '../queries/access_token.query.graphql';
import { GITLAB_GRAPHQL_BASE_URL } from '~/shared/constants';

const createCache = () => {
  const cache = new InMemoryCache();

  // set initial cache state
  cache.writeData({
    data: {
      selection: null,
      accessToken: null,
      currentUser: null,
    },
  });

  return cache;
};

export function createDefaultClient() {
  const cache = createCache();
  const authLink = setContext((_, { headers }) => {
    const { accessToken } = cache.readQuery({ query: accessTokenQuery }) || {};
    return {
      headers: {
        ...headers,
        Authorization: `Bearer ${accessToken}`,
      },
    };
  });

  return new ApolloClient({
    typeDefs,
    resolvers,
    cache,
    link: authLink.concat(createUploadLink({ uri: GITLAB_GRAPHQL_BASE_URL })),
  });
}

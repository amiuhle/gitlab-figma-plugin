import querystring from 'querystring';
import selectionQuery from '../queries/selection.query.graphql';
import accessTokenQuery from '../queries/access_token.query.graphql';
import { toIssue } from '../../utils/gitlab_utils';

export default {
  Mutation: {
    setSelection(_, { frames, components }, { cache }) {
      cache.writeQuery({
        query: selectionQuery,
        data: { selection: { __typename: 'Selection', frames, components } },
      });

      return { frames, components };
    },
    setAccessToken(_, { accessToken }, { cache }) {
      cache.writeQuery({
        query: accessTokenQuery,
        data: { accessToken },
      });

      return;
    },
  },
  Query: {
    async currentUser(_, __, { cache }) {
      const { accessToken } = cache.readQuery({ query: accessTokenQuery }) || {};
      if (!accessToken) throw new Error('Access token not set.');

      return fetch('https://gitlab.com/api/v4/user', {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
        .then(res => {
          if (res.status !== 200) {
            return res.json().then(({ message }) => {
              throw new Error(message);
            });
          }

          return res;
        })
        .then(res => res.json())
        .then(data => {
          const { id, name, username } = data;

          return {
            __typename: 'User',
            id,
            name,
            username,
          };
        });
    },
    async assignedIssues(_, __, { cache }) {
      const { accessToken } = cache.readQuery({ query: accessTokenQuery }) || {};
      if (!accessToken) throw new Error('Access token not set.');

      const query = {
        scope: 'assigned_to_me',
        order_by: 'updated_at',
        state: 'opened',
        per_page: 100,
        confidential: false,
      };
      return fetch(`https://gitlab.com/api/v4/issues?${querystring.stringify(query)}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
        .then(res => {
          if (res.status !== 200) {
            return res.json().then(({ message }) => {
              throw new Error(message);
            });
          }

          return res;
        })
        .then(res => res.json())
        .then(data => {
          return data.map(issue => {
            return {
              ...toIssue(issue, { projectId: issue.project_id }),
              __typename: 'Issue',
            };
          });
        });
    },
  },
};

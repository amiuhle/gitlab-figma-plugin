# GitLab Figma Plugin

> ⚠️ This plugin is in Beta.

The Official GitLab plugin to seamlessly upload your designs from Figma to GitLab issues (via [GitLab Design Management](https://docs.gitlab.com/ee/user/project/issues/design_management)).

## [User Guide](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/wikis/home)

For details on how to install and use the GitLab Figma Plugin, check out the [user guide](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/wikis/home).

## Developer guide

### Getting started

1. Install dependencies

   ```bash
   npm install
   ```

2. Build assets

   ```bash
   npm run build
   ```

   or, to watch files:

   ```bash
   npm run watch
   ```

#### Figma setup

Now that we've compiled the plugin, we need to add it to Figma.

1. In the hamburger menu top-left, navigate to _Plugins/Development/New plugin..._
2. Select this project's `manifest.json`
3. Follow prompts to finish plugin creation

#### Debugging tips

- To open the Figma development console: from the top menu, `Plugins > Development > Open Console`
- To re-run the last plugin command in Figma: `cmd+opt+p`

### Usage

Currently, the plugin only supports uploading of a design to a given Issue within GitLab.

#### Exporting a Figma Design to GitLab

1. Run the plugin (In the hamburger menu top-left, click _Plugins/Development/\<GitLab Plugin Name\>..._)

### Technical Notes

Figma plugins are built with 2 contexts in mind:

1. The Figma sandbox: where direct interaction with the Figma application happens
2. iframe - the UI for the plugin (i.e. the Vue app)

Read more about this [in the Figma documentation](https://www.figma.com/plugin-docs/how-plugins-run/)

![figma-sandbox-diagram](https://static.figma.com/uploads/04c4c6293fce2a7fe67bccd385ee5ab998705780)

#### `src/ui/`

The `ui/` directory holds all code related to the UI. This is a single page application implemented in Vue. It is entirely `javascript` and Vue single-file-components (for now).

#### `src/figma`

The `figma/` directory holds all code that will run in the Figma sandbox. It is entirely `typescript`.
